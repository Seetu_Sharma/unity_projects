﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject WinText;
    int score = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Score() {
        score++;
        if (score >= 2)
        {
            Win();
        }
    }
    public void Win() {
        WinText.SetActive(true);
    
        
    }
    public void Restart()
    {
        SceneManager.LoadScene("Game");
    }


}
