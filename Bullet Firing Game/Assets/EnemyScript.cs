﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    float enemySpeed = 300;
    bool shoot = false;

    public GameObject enemy;
    public Transform enemyPos;
    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            shoot = true;
        }
    }
    void FixedUpdate()
    {
        if (shoot)
        {
            Shoot();
            shoot = false;

        }
    }
    void Shoot()
    {
        GameObject bulletSpawn = Instantiate(enemy, enemyPos.position, enemy.transform.rotation);
        bulletSpawn.GetComponent<Rigidbody>().velocity = new Vector3(enemySpeed,0,0);
        
    }
}
