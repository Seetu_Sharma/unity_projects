﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    int enemyKilled = 0;
    // Start is called before the first frame update
    void Start()
    {
        enemyKilled++;
        Debug.Log("Enemy Killed :" + enemyKilled);

    }

    // Update is called once per frame
    void Update()
    {
        
        
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Destroy(collision.gameObject);
            
        }
    }
    
}
