﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    float bulletSpeed=150;
    bool shoot = false;
   
    public GameObject bullet;
    public Transform bulletPos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            shoot = true;
        }
        
    }
    void FixedUpdate()
    {
        if (shoot)
        {
            Shoot();
            shoot = false;

        }
    }
    void Shoot() {
        GameObject bulletSpawn =  Instantiate(bullet, bulletPos.position, bullet.transform.rotation);
        bulletSpawn.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, bulletSpeed);

    
    }
}
