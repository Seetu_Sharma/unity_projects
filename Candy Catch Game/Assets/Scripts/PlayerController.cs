﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour

{
    public bool canMove = false;
    [SerializeField]
    float moveSpeed;
    [SerializeField]
    float maxPos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            Move();
        }
        
    }
    private void Move()
    {
        float inputx = Input.GetAxis("Horizontal");
        transform.position += Vector3.right * inputx * moveSpeed * Time.deltaTime;
        float xPos =  Mathf.Clamp(transform.position.x, -maxPos, maxPos);
        transform.position = new Vector3(xPos, transform.position.y, transform.position.z);

        
    }
}
